from time import sleep
import os
from spherov2 import scanner
from spherov2.sphero_edu import SpheroEduAPI
from spherov2.types import Color
import pyyoutube

channel_id = "UCiGbltynM8mLuPOogDpJU3g"

# LED colours
off   = Color(0, 0, 0)
red   = Color(255, 0, 0)
green = Color(0, 255, 0)
blue  = Color(0, 0, 255)
white = Color(255, 255, 255)

def get_yt_sub_count(yt, channel_id):
    channel_by_id = yt.get_channel_info(channel_id=channel_id)
    return int(channel_by_id.items[0].statistics.subscriberCount)

def bb8_celebrate(bb8):
    with SpheroEduAPI(bb8) as droid:
        droid.fade(white, red, 0.5)
        droid.roll(45, 128, 0.1)
        droid.roll(-45, 128, 0.1)
        droid.fade(red, green, 0.5)     
        droid.fade(green, blue, 0.5)
        droid.spin(405, 1) # that's a good trick
        droid.fade(blue, off, 0.5)

if __name__ == "__main__":
    
    yt = pyyoutube.Api(api_key=os.environ['YOUTUBE_KEY'])
    bb8 = scanner.find_BB8()

    previous_sub_count = 0

    while True: # run forever
        latest_sub_count = get_yt_sub_count(yt, channel_id)

        if latest_sub_count > previous_sub_count:
            print(f"Now at {latest_sub_count} subs!") 
            bb8_celebrate(bb8)

        previous_sub_count = latest_sub_count
        sleep(10)
