# BB-8 Sphero Youtube Subscription Notifier

As seen on youtube! Requires Python 3.7 or later.

## To Download
Run `git clone https://gitlab.com/ejstreet/bb8-sphero.git`

## To Install Requirements
Run `pip install -r requirements.txt`. 

## To Run
You will need a Google developer account and a youtube API token as described [here](https://developers.google.com/youtube/v3/getting-started).

With your token acquired, run `export YOUTUBE_KEY='TOKEN__COPIED_FROM_GOOGLE'`.

Finally, run `python app.py` from inside this folder to start the program.